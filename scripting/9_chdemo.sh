echo "enter a char"
read ch

case $ch in
	[a-z]|[A-Z]) echo "alphabet";;
	[0-9]) echo "numeric digit";;
	?) echo "unknown single char";;
	*) echo "unknown multiple char";;
esac

echo "enter another char(alphbet)"
read ch

case $ch in
	[aeiou]|[AEIOU]) echo "$ch is a vowel";;
	?) echo "consonant";;
esac
